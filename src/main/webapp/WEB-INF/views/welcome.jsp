<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<!-- This is EL Syntax -->
	<!-- We have added customer in sessionScope, but we are accessing customer object directly without specifying in which scope it is in  -->
	<!-- Hence jsp container will try to find it in hirarchical fashion -->
	<!-- First it will try to find it in pageScope -->
	<!-- If not found in pageScope then it will try to find it in requestScope -->
	<!-- If not found in requestScope then it will try to find it in sessionScope (Model ar nothing but request scope)-->
	<!-- If not found in sessionScope then it will try to find it in applicationScope -->
	<h1>Welcome ${customer.name}</h1>

	<!-- If we want to specify scope explicitly then use below syntax -->
	<%-- <h1>Welcome ${sessionScope.customer.name}</h1> --%>

</body>
</html>