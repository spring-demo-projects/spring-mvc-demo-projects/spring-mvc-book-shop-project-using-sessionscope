package com.cdac.model;

import java.util.Arrays;

public class BookModel {
	private String[] book;

	public BookModel() {
		this.book = new String[0];
	}

	public BookModel(String[] book) {
		super();
		this.book = book;
	}

	public String[] getBook() {
		return book;
	}

	public void setBook(String[] book) {
		this.book = book;
	}

	@Override
	public String toString() {
		return "BookModel [book=" + Arrays.toString(book) + "]";
	}

}
